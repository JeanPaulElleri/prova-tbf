package model;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


import java.time.LocalDate;
import java.time.Period;


/**
 * class for model test v15.
 */
public class ModelTest {

    private static final int INTERO = 5;
    private static final int INTERO2 = 9;

    /**
     * Test 1.
     */
    @Test
    public void test() {
        final LocalDate primasemina = LocalDate.of(2017, 05, 20);
        System.out.println("Data semina: " + primasemina);

        final LocalDate now = LocalDate.now();
        System.out.println("Data oggi: " + now);


        Period periodo = Period.between(primasemina, now);
        System.out.println("tempo trascorso: " + periodo.getMonths() + " mesi e " + periodo.getDays() + " giorni");

        for (final Color c: Color.values()) {
            System.out.print(c + " ");
        }
        periodo = periodo.normalized();
        //periodo = periodo.negated();
        if (periodo.isNegative()) {
            fail("negative period");
        }
    }



    /**
     * Test 2.
     */
    @Test
    public void test2() {
        Period periodo2 = Period.ofDays(2);
        periodo2 = periodo2.plusDays(2);
        assertEquals(periodo2.plusDays(INTERO).getDays(), INTERO2);
    }

    /**
     * Test 3.
     */
    @Test
    public void test3() {
        final Rosa r1 = new Rosa();
        final Narciso n1 = new Narciso();

        assertEquals(r1.getColor(), Color.RED);

        n1.setCondition(Condition.BUD);
        n1.setWatered(true);
        n1.setDate(LocalDate.now().minusDays(INTERO)); //semina 5 giorni fa
        n1.setWatered(false);

        System.out.println("\nNarciso: cond-water-color : " + n1.getCondition() + n1.isWatered() + n1.getColor());
        System.out.println("\nNarciso:semina : " + n1.getDate() + " giorni dalla semina: " + n1.getAge().getDays());

    }

}
