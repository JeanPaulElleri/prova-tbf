package model;

import java.time.Period;

/**
 * implements plant Narciso.
 */
public class Narciso extends PlantImp {


    private static final int FIORITURA = 12; //giorni di fioritura

    private static final String NAME = "Narciso";
    private static final Color COLORE = Color.YELLOW;
    private static final String DESCRIPTION = "Narcisogiallo"; //aggiungere descrizione da file
    private static final Period BTIME = Period.ofDays(FIORITURA);


    /**
     * serial number.
     */
    private static final long serialVersionUID = -7621827224672334219L;



    /**
     * costruttore.
     */
    public Narciso() {
        super(NAME, COLORE, DESCRIPTION, BTIME);
    }
}
