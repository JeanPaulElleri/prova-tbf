package model;

/**
 * implementation of greenhouse.
 */
public class GreenhouseImp implements Greenhouse {

    private int numFlowers;
    private static final int SPACEAVAIABLE = 40;

    @Override
    public int getNumFlowers() {
        return numFlowers;
    }

    @Override
    public void setNumFlowers(final int numFlowers) {
        this.numFlowers = numFlowers;
    }

    @Override
    public int getNumFreeSpaces() {
        return SPACEAVAIABLE - numFlowers;
    }

}
