package model;

import java.time.Period;

/**
 * implements plant rose.
 */
public class Rosa extends PlantImp {


    private static final int FIORITURA = 13; //giorni di fioritura

    private static final String NAME = "Rosa";
    private static final Color COLORE = Color.RED;
    private static final String DESCRIPTION = "rosarossa"; //aggiungere descrizione da file
    private static final Period BTIME = Period.ofDays(FIORITURA);


    /**
     * serial number.
     */
    private static final long serialVersionUID = -7621827224672334219L;



    /**
     * costruttore.
     */
    public Rosa() {
        super(NAME, COLORE, DESCRIPTION, BTIME);
    }
}
