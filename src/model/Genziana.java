package model;

import java.time.Period;

/**
 * implements plant Genziana.
 */
public class Genziana extends PlantImp {


    private static final int FIORITURA = 5; //giorni di fioritura

    private static final String NAME = "Genziana";
    private static final Color COLORE = Color.BLUE;
    private static final String DESCRIPTION = "genzianablu"; //aggiungere descrizione da file
    private static final Period BTIME = Period.ofDays(FIORITURA);


    /**
     * serial number.
     */
    private static final long serialVersionUID = -7621827224672334219L;



    /**
     * costruttore.
     */
    public Genziana() {
        super(NAME, COLORE, DESCRIPTION, BTIME);
    }
}
