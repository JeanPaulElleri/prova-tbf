package model;

import java.time.Period;

/**
 * implements plant Orchidea.
 */
public class Orchidea extends PlantImp {


    private static final int FIORITURA = 27; //giorni di fioritura

    private static final String NAME = "Orchidea";
    private static final Color COLORE = Color.PURPLE;
    private static final String DESCRIPTION = "Orchideaviola"; //aggiungere descrizione da file
    private static final Period BTIME = Period.ofDays(FIORITURA);


    /**
     * serial number.
     */
    private static final long serialVersionUID = -7621827224672334219L;



    /**
     * costruttore.
     */
    public Orchidea() {
        super(NAME, COLORE, DESCRIPTION, BTIME);
    }
}
