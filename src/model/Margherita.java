package model;

import java.time.Period;

/**
 * implements plant Margherita.
 */
public class Margherita extends PlantImp {


    private static final int FIORITURA = 8; //giorni di fioritura

    private static final String NAME = "Margherita";
    private static final Color COLORE = Color.WHITE;
    private static final String DESCRIPTION = "Margheritabianca"; //aggiungere descrizione da file
    private static final Period BTIME = Period.ofDays(FIORITURA);


    /**
     * serial number.
     */
    private static final long serialVersionUID = -7621827224672334219L;



    /**
     * costruttore.
     */
    public Margherita() {
        super(NAME, COLORE, DESCRIPTION, BTIME);
    }
}
