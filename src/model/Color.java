package model;

/**
 * color of flower.
 *
 */
public enum Color {

    /**
     * red of rose.
     */
    RED,

    /**
     * pink of garofano.
     */
    PINK,

    /**
     * blue of genziana.
     */
    BLUE,

    /**
     * white of margherita.
     */
    WHITE,

    /**
     * yellow of narciso.
     */
    YELLOW,

    /**
     * purple of orchidea.
     */
    PURPLE;


}
