package model;

import java.time.LocalDate;
import java.time.Period;

/**
 * implements the plant.
 */
public abstract class PlantImp implements Plant {

    private static final long serialVersionUID = -675058626311300658L;

    private final String name;
    private final Color colore;
    /**
     * time for the blooming
     */
    private final Period bTime;

    private final String description;

    /**
     * the condition : seed bud flower withered
     */
    private boolean watered;
    private Condition condizione;
    /**
     * the date the flower is planted
     */
    private LocalDate sowing; //differenza tra Date e LocalDate?




    /**
     * 
     * @param name name of the flower
     * @param colore color of the flower
     * @param description description of the flower
     * @param bTime blooming time of the flower
     */
    public PlantImp(final String name, final Color colore, final String description, final Period bTime) {
        this.name = name;
        this.colore = colore;
        this.description = description;
        this.bTime = bTime;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public Color getColor() {
        // returns the color
        return colore;
    }

    @Override
    public Period getBloomigTime() {
        return bTime;
    }

    @Override
    public String getDescription() {
        return description;
    }


    @Override
    public boolean isWatered() {
        return watered;
    }

    @Override
    public void setWatered(final boolean watered) {
        this.watered = watered;

    }

    @Override
    public Condition getCondition() {
        return condizione;
    }

    @Override
    public void setCondition(final Condition condizione) {
        this.condizione = condizione;

    }

    @Override
    public LocalDate getDate() {
        return sowing;
    }

    @Override
    public void setDate(final LocalDate data) {
        this.sowing = data;
    }

    @Override
    public Period getAge() {
        return Period.between(sowing, LocalDate.now());
    }





}
