package model;

/**
 * the state of the plant.
 *
 */
public enum Condition {

    /**
     * seme.
     */
    SEED,

    /**
     * germoglio.
     */
    BUD,

    /**
     * fiore.
     */
    FLOWER,

    /**
     * appassito.
     */
    WITHERED; 
}
