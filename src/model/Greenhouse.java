package model;

/**
 * plants store.
 */
public interface Greenhouse {

    /**
     * @return the total number of flowers
     */
    int getNumFlowers();


    /**
     * sets the number of flowers.
     * @param numFlowers number of flowers
     */ 
    void setNumFlowers(int numFlowers);

    /**
     * @return the free space
     * (the flowers available)
     */
    int getNumFreeSpaces();
}
