package model;

import java.time.Period;

/**
 * implements plant garofano.
 */
public class Garofano extends PlantImp {


    private static final int FIORITURA = 22; //giorni di fioritura

    private static final String NAME = "Garofano";
    private static final Color COLORE = Color.PINK;
    private static final String DESCRIPTION = "garofanorosa"; //aggiungere descrizione da file
    private static final Period BTIME = Period.ofDays(FIORITURA);


    /**
     * serial number.
     */
    private static final long serialVersionUID = -7621827224672334219L;



    /**
     * costruttore.
     */
    public Garofano() {
        super(NAME, COLORE, DESCRIPTION, BTIME);
    }
}
