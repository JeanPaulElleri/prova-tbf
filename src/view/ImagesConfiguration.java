package view;

import java.awt.Image;
import java.util.Optional;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/*
 * This class loads the images from a path,
 * to simplify the process of getting the images 
 * to the other classes.
*/

/*
 * non mettere questa classe in un package a parte, suddivisione inutile
 * secondo me e finiamo per assomigliare troppo 
 * al progetto della spiaggia
 */
public final class ImagesConfiguration {

    private static final String PATH = "/";
    private final static ImagesConfiguration SINGLETON = new ImagesConfiguration();
    private final Optional<Icon> roseIcon;
    private final Optional<Icon> tulipIcon;
    private final Optional<Icon> sunflowerIcon;
    private final Optional<Icon> lilyIcon;
    private final Optional<Icon> genericPlantIcon;
    
    /*
     * queste cose le devi guardare te, a me per ora
     * interessa solo delle icone che ci sono sopra
     */
    /*
     * ICONE MENU, da rivedere
     */
    private final Optional<Icon> mainviewIcon; //principale
    private final Optional<Icon> settingsIcon; //impostazioni
    private final Optional<Icon> employeesIcon; //impiegati
    private final Optional<Icon> storageIcon; //magazzino
    private final Optional<Icon> infoIcon; //informazioni
    
    //immagine prato
    private final Optional<Image> fieldImage;

    private ImagesConfiguration() {

        this.roseIcon = createIcon(PATH.concat("Rose-icon.png"));
        this.tulipIcon = createIcon(PATH.concat("Tulip-icon.png"));
        this.sunflowerIcon = createIcon(PATH.concat("Sunflower-icon.png"));
        this.lilyIcon = createIcon(PATH.concat("Lily-icon.png"));
        this.genericPlantIcon = createIcon(PATH.concat("GenericPlant-icon.png"));
        
        /*
         * ICONE MENU
         */
        this.mainviewIcon = createIcon(PATH.concat("Main-View-icon.png"));
        this.settingsIcon = createIcon(PATH.concat("Settings-icon.png"));
        this.employeesIcon = createIcon(PATH.concat("Employees-icon.png"));
        this.storageIcon = createIcon(PATH.concat("Storage-icon.png"));
        this.infoIcon = createIcon(PATH.concat("Info-icon.png"));
        
        //immagine prato
        this.fieldImage = createImage(PATH.concat("Field-Image.png"));
        
    }

    private Optional<Image> createImage(String path) {
        return Optional.ofNullable(new ImageIcon(this.getClass().getResource(path)).getImage());
    }

    private Optional<Icon> createIcon(final String path) {
        return Optional.ofNullable(new ImageIcon(this.getClass().getResource(path)));
    }

    public Optional<Icon> getRoseIcon() {
        return this.roseIcon;
    }

    public Optional<Icon> getTulipIcon() {
        return this.tulipIcon;
    }

    public Optional<Icon> getDaisyIcon() {
        return this.sunflowerIcon;
    }

    public Optional<Icon> getLilyIcon() {
        return this.lilyIcon;
    }

    public Optional<Icon> getGenericPlantIcon() {
        return this.genericPlantIcon;
    }
    
    /*
     * aggiunti get che non ti riguardano
     */
    public Optional<Icon> getMainviewIcon() {
    	return this.mainviewIcon;
    }
    
    public Optional<Icon> getSettingsIcon() {
    	return this.settingsIcon;
    }
    
    public Optional<Icon> getEmployeesIcon() {
    	return this.employeesIcon;
    }
    
    public Optional<Icon> getStorageIcon() {
    	return this.storageIcon;
    }
    
    public Optional<Icon> getInfoIcon() {
    	return this.infoIcon;
    }
    
    public Optional<Image> getFieldImage() {
    	return this.fieldImage;
    }
    
    
    public static ImagesConfiguration getConfig() {
        return ImagesConfiguration.SINGLETON;
    }

}