package view;

import java.awt.Component;

import javax.swing.JOptionPane;

/**
 * An interface representing a generic view, that can be 
 * easily controlled by its observer through its basic methods 
 * 
 *
 */
public interface View {
    
    /**
     * Initializes view
     */
    void init();
    
    /**
     * Shows an error message
     * 
     * @param msg
     *            the message that will be shown
     * @param view
     *            the graphic element where the message will be shown
     */
    default public void logError(String msg, Component view) {
        JOptionPane.showMessageDialog(view, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     * Shows a generic message
     * 
     * @param msg
     *            the message that will be shown
     * @param view
     *            the graphic element where the message will be shown
     */
    default void showMessage(String msg, Component view) {
        JOptionPane.showMessageDialog(view, msg);
    }

    
}

