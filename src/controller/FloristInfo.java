package controller;

import java.util.function.Predicate;

/**
 * enum that contains all the florist possible field
 *
 */
public enum FloristInfo {
	NAME("Name: ", n->!n.isEmpty() && !n.matches(".*\\d+.*"), "error: wrong name"),
	SURNAME("Surname: ", s->!s.isEmpty() && !s.matches(".*\\d+.*"), "error: wrong surname"),
	FISCALCODE("Fiscal code: ", fc->fc.matches(".*\\d+.*[0-9]*") && fc.length()==16, "error: wrong fiscal codess"),
	ADDRESS("Address: ", a->!a.isEmpty(), "error: wrong address"),
	PHONENUM("Phone number: ", pn->{ 
		return pn.isEmpty() ? true : pn.chars().allMatch(c->Character.isDigit(c));
	}, "error: wrong phone number"),
	MAIL("E-Mail: ",em->{
		return em.isEmpty() ? true : em.contains("@");
	},"error: wrong E-Mail");
	
	private final String info;
	private final Predicate<String> validity;
	private final String errorMsg;
	
	private FloristInfo(String info, Predicate<String> validity, final String errorMsg) {
		this.info = info;
		this.validity = validity;
		this.errorMsg = errorMsg;
	}

	public String getInfo() {
		return this.info;
	}
	
	 public Predicate<String> getValidity() {
		return this.validity;
	 }

	public String getErrorMsg() {
		return this.errorMsg;
	}
}