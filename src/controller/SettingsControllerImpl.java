package controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.omg.CORBA.PRIVATE_MEMBER;
import org.omg.CORBA.portable.ValueInputStream;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;
import com.google.common.base.FinalizablePhantomReference;
import view.SettingsView;

public class SettingsControllerImpl implements SettingsController{

	private final static int MAXFLOWERNUM = 40;
	private final static SettingsController SINGLETON = new SettingsControllerImpl();
	private final static String PATH_CONFIG_FILE = System.getProperty("user.home")
			+ System.getProperty("file.separator")
			+ ".thebloomingflorist"
			+ System.getProperty("file.separator")
			+ "config.dat";
	private final static Map<ConfigParam, String> INIT;
	static {
		INIT = new HashMap<>();
		INIT.put(ConfigParam.ROSENUM, "10");
		INIT.put(ConfigParam.TULIPNUM, "10");
		INIT.put(ConfigParam.DAISYNUM, "10");
		INIT.put(ConfigParam.LILYNUM, "5");
		INIT.put(ConfigParam.GENERCICPTNUM, "5");
	}
	
	private SettingsView view;
	private SettingsControllerImpl() {
	}

	@SuppressWarnings("unchecked")
	public Map<ConfigParam, String> getGreenhouseSettings() {
		
		/*
		try(final BufferedReader in = new BufferedReader(new FileReader(PATH_CONFIG_FILE))) {
            final Yaml yml = new Yaml();
            final Map<String,String> loaded = (Map<String,String>)yml.load(in);
            return Stream.of(ConfigParam.values())
                         .collect(Collectors.toMap(cf->cf, cf->loaded.get(cf.getName())));
        } catch (final IOException exc) {
            return INIT;
        }
		*/
		
		/****************** FANNO LA STESSA COSA O NO????? **************************/
		
		/*
		final Map<ConfigParam, String> load = new HashMap<>();
        try (ObjectInputStream in = new ObjectInputStream(
                new BufferedInputStream(new FileInputStream(PATH_CONFIG_FILE)));) {
            
            final Map<ConfigParam,String> loaded = (Map<ConfigParam, String>) in.readObject();
            return Stream.of(ConfigParam.values())
                    .collect(Collectors.toMap(cf->cf, cf->loaded.get(cf.getName())));
        } catch (final IOException | ClassNotFoundException e) {
            System.out.println("Impossible loading");
        }
        return load;
		*/
		return null;
	}
	
	@Override
	public void save(final List<String> numPlant) {
		if(this.checkFields(numPlant)) {
			try(final BufferedWriter out = new BufferedWriter(new FileWriter(PATH_CONFIG_FILE))) {
				final DumperOptions options = new DumperOptions();
				options.setDefaultFlowStyle(FlowStyle.BLOCK);
				final Yaml yml = new Yaml(options);
				final Map<String , String> toWrite = new HashMap<>();
				for(int i=0; i<numPlant.size(); i++) {
					toWrite.put(ConfigParam.values()[i].getName(), numPlant.get(i));
				}
				yml.dump(toWrite, out);				
			} catch (final IOException e) {
				this.view.showError("File ConfigParam.yml not found",this.view);
			}
			final View old = GreenhouseControllerImpl.getInstance().getBeachView();
            GreenhouseControllerImpl.getInstance().update();
            MainControllerImpl.getSingleton().updateView(GreenhouseControllerImpl.getInstance().getGreenhouseView(),old);
            MainControllerImpl.getSingleton().show(GreenhouseControllerImpl.getInstance().getGreenhouseView());			
		} else {
			this.view.showError("Invalid insertion", this.view);
		}
	}

	/*
	 * 
	 */
	private boolean checkFields(final List<String> numPlant) {
		final List<Integer> tocheck = new ArrayList<>();
		for(String s : numPlant) {
			try {
				tocheck.add(Integer.parseInt(s));
			} catch (NumberFormatException exc) {
				return false;
			}
		}
		if (tocheck.stream().anyMatch(i->i<0) ||
				(tocheck.get(ConfigParam.ROSENUM.ordinal())+
				tocheck.get(ConfigParam.TULIPNUM.ordinal())+
				tocheck.get(ConfigParam.DAISYNUM.ordinal())+
				tocheck.get(ConfigParam.LILYNUM.ordinal())+
				tocheck.get(ConfigParam.GENERCICPTNUM.ordinal()))>MAXFLOWERNUM) {
			return false;
		} else {
			return true;
		}
	}
	
	
	@Override
	public SettingsView getView() {
		return this.view;
	}

	@Override
	public void setView(SettingsView view) {
		this.view = view;
		this.view.init();//metodo della view 
	}
	
	/*
	 * 
	 */
	public static SettingsController getSingleton() {
		return SINGLETON;
	}

}
