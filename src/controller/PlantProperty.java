package controller;

import model.Daisy;
import model.GenericPlant;
import model.Lily;
import model.Plant;
import model.Rose;
import model.Tulip;
import view.ImagesConfiguration;
import view.SettingsView;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import javax.swing.Icon;

public interface PlantProperty {
	
	enum PlantType {
		ROSE("rose", ImagesConfiguration.getConfig().getRoseIcon(),0,40,pt->new Rose(pt)), 
	    TULIP("tulip", ImagesConfiguration.getConfig().getTulipIcon(),0,40,pt->new Tulip(pt)), 
	    DAISY("daisy", ImagesConfiguration.getConfig().getDaisyIcon(),0,40,pt->new Daisy(pt)), 
	    LILY("lily", ImagesConfiguration.getConfig().getLilyIcon(),0,40,pt->new Lily(pt)),
		GENERIC("generic plant type",ImagesConfiguration.getConfig().getGenericPlantIcon(),0,40,pt->new GenericPlant(pt));		
		
		
		private final String name;
	    private final Optional<Icon> iconPath;
	    private final int startValue,maxSelectable;
	    private final Function<Plant, Plant> decoration;

	    private PlantType (final String name, final Optional<Icon> icon, final int startValue, 
	            final int maxSelectable, final Function<Plant, Plant> decoration) {
	        this.name = name;
	        this.iconPath = icon;
	        this.startValue = startValue;
	        this.maxSelectable = maxSelectable;
	        this.decoration = decoration;
	    }
	    
	    public String getName() {
	        return this.name;
	    }
	    
	    public Optional<Icon> getIconPath() {
	        return this.iconPath;
	    }
	    
	    public int getMaxSelectable() {
	        return this.maxSelectable;
	    }

	    public Function<Plant, Plant> getDecoration() {
	        return this.decoration;
	    }

	    public int getStartValue() {
	        return startValue;
	    }
	}
}
