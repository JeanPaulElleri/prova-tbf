package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import javax.swing.Icon;
import org.apache.commons.io.IOUtils;
import view.ImagesConfiguration;
import view.MainTabbedPane;
import view.View;

public class MainControllerImpl implements MainController {

	private final static MainController SINGLETON = new MainControllerImpl();
	private MainTabbedPane frame;
	//percorso cartella principale di progetto
	private final static String MAIN_DIRECTORY = System.getProperty("user.home")+
			System.getProperty("file.separator")+".thebloomingflorist";
	//percorso di tutti i file di configurazione
	private final static List<String> CONF_FILES = Arrays.asList("plantsState.dat",
																 "employee.dat",
																 "config.dat");//conviene?
	
	private MainControllerImpl() {
		try {
			this.install();
		} catch(IOException e) {
			System.out.println("installation failed");
		}
		
		this.frame = new MainTabbedPaneImpl();
		final GreenhouseView greenhouseView = GreenhouseControllerImpl.getInstance().getGreenhouseView();
		final Icon greenhouseIcon = ImagesConfiguration.getConfig().getFreeIcon().get();
		this.frame.addTab((JPanel)greenhouseView,"Greenhouse",greenhouseViewIcon);
		
		final Icon settingsIcon = ImagesConfiguration.getConfig().getSettingsIcon().get();
		SettingsControllerImpl.getSingleton().setView(new SettingsView());
		SettingsControllerImpl.getSingleton().getView().init();
		this.frame.addTab(SettingsControllerImpl.getSingleton().getView(),"Settings",settingsIcon);
		
        final Icon employeeIcon = ImagesConfiguration.getConfig().getEmployeeIcon().get();
        this.frame.addTab(EmployeeControllerImpl.getInstance().getGuestView(), "Employees", employeeIcon);
	}
	
	/**
	 * This private method create a new main program folder if it's absent
	 * 
	 */
	private static void createMainFolder() {
		File directory = new File(MAIN_DIRECTORY);
		if(directory.exists()) {
			System.out.println("Directory already exists ...");
		} else {
			System.out.println("Directory not exists, creating ...");
			if(directory.mkdir()) {
				System.out.println("Successfully created new directory");
			} else {
				System.out.println("Error creating new directory");
			}
		}
	}
	
	/**
	 * This private method copy and load all the configuration files
	 * 
	 */
	private void install() {
		createMainFolder();
		for(String s : CONF_FILES) {
			final String path = MAIN_DIRECTORY + System.getProperty("file.separator") + s;
			if(!Files.exists(Paths.get(path))) {
				try {
					IOUtils.copy(this.getClass().getResourceAsStream("/"+s),
							new FileOutputStream(path));
				} catch (final NullPointerException | IOException exc) {
					System.out.println(s + "not found");
				}
			}
		}
	}

	@Override
	public void startApp() {
		this.frame.init();
		
	}

	@Override
	public void show(final View view) {
		this.frame.goToTab(view);		
	}

	@Override
	public void updateView(View newView, View oldView) {
        this.frame.replaceTab(newView, oldView);
	}
	
	public static MainController getSingleton() {
        return SINGLETON;
    }

}
