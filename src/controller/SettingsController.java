package controller;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Range;

import view.SettingsView;

public interface SettingsController {
	
	enum ConfigParam {
        ROSENUM("Number of Rose", Range.closed(1, 40)),
        TULIPNUM("Number of Tulip", Range.closed(1, 40)),
        DAISYNUM("Number of Daisy", Range.closed(1, 40)),
        LILYNUM("Number of Lily", Range.closed(1, 40)),
        GENERCICPTNUM("Number of generic plant type", Range.closed(1, 40));
        
        private final String name;
        private final Range<Integer> range;
        
        private ConfigParam(final String name, final Range<Integer> range) {
            this.name = name;
            this.range = range;
        }

        public String getName() {
            return name;
        }

        public Range<Integer> getRange() {
            return range;
        }
    }
			
	/**
	 * This method load the previous configuration file
	 * 
	 * @return a map loaded by config.yml file 
	 */
	Map<ConfigParam, String> getGreenhouseSettings();
	
	/**
	 * This method save the changes made if they are correct
	 * 
	 * @param numPlant
	 */
	void save(List<String> numPlant);
	
	/**
	 * This method return a SettingsView
	 * 
	 * @return SettingsView
	 */
	SettingsView getView();
	
	/**
	 * This method change the view and initialize it
	 * 
	 * @param view
	 */
	void setView(SettingsView view);
}
