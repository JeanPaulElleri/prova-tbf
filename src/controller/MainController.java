package controller;

import view.View;

public interface MainController {
	/**
	 * This method starts the application and initializes fields.
	 * 
	 */
	void startApp();

	/**
	 * This method shows the passed view
	 * 
	 * @param view
	 *
	 */
	void show(View view);

	/**
	 * This method replaces an old view with a new one
	 * 
	 * @param newView
	 * @param newView
	 * 
	 */
	void updateView(final View newView, final View oldView);
}
