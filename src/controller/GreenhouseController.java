package controller;

import java.sql.Date;
import java.util.List;
import model.Plant;

public interface GreenhouseController {

	/**
	 * 
	 * @return List of @plant
	 */
	List<Plant> getPlant();
	//non so se getplant vada bene, loro fanno getombrellone 
	
	/**
	 * 
	 * @param from
	 * @param to
	 */
	void selectDates(Date from, Date to);
	
	/**
	 * 
	 * @param pos
	 */
	void plantFlower(int pos);
	
	/**
	 * This method updates @GreenhouseView
	 */
	void computeGreenhouse();
	
	
	
	
	
}
